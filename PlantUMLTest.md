Plant UML Test
==============

```plantuml
@startuml
skinparam componentStyle rectangle

package "Front-end" {
  [Third UI]
  [Second UI]
  [First UI]
  [GraphQL]
  note right of GraphQL
    Custom queries by caller
    (One node in larger GraphQL system)
  end note
}
package "Services" {
  [endpoint3]
  [endpoint2]
  [endpoint1]
  [Product Processor]
  [Mongo]
  [Webservice]
  [Oracle]
  [Other]
}
note bottom of Services
  Black-box system 
  (from UI perspective)
end note


[First UI] -- [GraphQL]
[Second UI] -- [GraphQL]
[Third UI] -- [GraphQL]
[GraphQL] -- [endpoint1]
[GraphQL] -- [endpoint2]
[GraphQL] -- [endpoint3]
[endpoint1] -- [Product Processor]
[endpoint2] -- [Product Processor]
[endpoint3] -- [Product Processor]
[Product Processor] -- [Mongo]
[Product Processor] -- [Webservice]
[Product Processor] -- [Oracle]
[Product Processor] -- [Other]

@enduml
```
